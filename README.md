RMDX
====

RMDX - is an XML/A OLAP MDX interface

Based on the library written by Piers Harding https://github.com/piersharding/RMDX

There are some modifications to support multidimensional columns and utf-8 character encoding.
Later changes might improve the structure of the data frame, improve column names and maybe create time series directly when possible.

## Summary

This module is an XML/A OLAP interface for MDX, specifically Mondrian, but should support others eg: SAP HANA.

### Prerequisites:
Please insure that httr, devtools and XML are installed:
```R
install.packages('XML', 'httr', 'devtools')
```

### Installation:
```R
library(devtools)
install_bitbucket('capia_as/rmdx')
```

Define credentials in an external file like ~/.Rprofile which is run on every startup of R. This is both convenient and avoids accidental publishing of credentials, as there is no need to include this in every file.
```R
#~/.Rprofile
capia <- data.frame(username = 'username', password = 'password', stringsAsFactors=FALSE)
# Important that file ends with a new line
```

On top of any projects, put the following lines:
```R
library(RMDX)
conn <- RMDX(connentaho='https://example.com/xmla/', userid=capia$username, password=capia$password)
```

### Examples:
#### Example table
```R
library(knitr)
cod_exp_by_country <- mdxquery(conn, "Trade", "Trade",
  "WITH SET [~ROWS] AS {[Partner].[Partner].[PartnerCode].Members} 
   SELECT NON EMPTY {[Measures].[Value]} ON COLUMNS, 
   NON EMPTY [~ROWS] ON ROWS 
   FROM [NoTrade] 
   WHERE ({[Flow].[Flow].[export]} * {[Specie].[Specie].[torsk]} * {[Time].[Time].[2015]})")
colnames(cod_exp_by_country)[1] <- "Country"
colnames(cod_exp_by_country)[2] <- "Iso3"
colnames(cod_exp_by_country)[3] <- "Value"
kable(head(cod_exp_by_country))
```

|Country   |Iso3 |    Value|
|:---------|:----|--------:|
|Angola    |AGO  |  1037364|
|Argentina |ARG  |   376476|
|Australia |AUS  |  5213197|
|Barbados  |BRB  |    99998|
|Belgia    |BEL  | 36977902|
|Benin     |BEN  |   452189|

#### Example plot with forecast
```R
# Load libraries
library(forecast)
library(ggplot2)
library(scales)
# Load dataset
result.raw <- mdxquery(conn, "Trade", "Trade", 
  "WITH SET [~COLUMNS] AS {[Specie].[Specie].[torsk]} 
  SET [~ROWS] AS Hierarchize({{[Time].[Time].[Year].Members}, {[Time].[Time].[Month].Members}})
  SELECT NON EMPTY CrossJoin([~COLUMNS], {[Measures].[Value]}) ON COLUMNS, 
  NON EMPTY [~ROWS] ON ROWS FROM [NoTrade] 
  WHERE CrossJoin({[Flow].[Flow].[export]}, {[Storage].[Storage].[fersk]})")
# Convert to time series needed by forecast
result.timeSeries <- ts(result.raw[, 4], start = c(1988, 1), frequency = 12)
# Run forecast
result.forecast <- forecast(auto.arima(result.timeSeries), level = 75)
# Plot results
autoplot(result.forecast) +
  ggtitle('Norwegian fresh cod exports forecast by value') +
  ylab('Value') +
  scale_y_continuous(labels = scales::comma)
```

![Norwegian exports of fresh cod forecast plot](https://bitbucket.org/capia_as/rmdx/raw/master/no_cod_exports_example_plot.png)

#### Example plot with melted table
```R
library(reshape2)
result.raw <- mdxquery(conn, "Trade", "Trade",
  "WITH 
  MEMBER [Specie].[Oppdrett] AS [Specie].[Specie].[Specie].[atlanterhavslaks] 
  MEMBER [Specie].[Konvensjonell] AS Aggregate(Except([Specie].[Specie].[Specie].Members, [Specie].[Specie].[Specie].[atlanterhavslaks])) 
  SET [~ROWS] AS TopCount({[Partner].[Partner].[PartnerCode].Members}, 30, [Measures].[Value]) 
  SELECT NON EMPTY CrossJoin({[Specie].[Oppdrett], [Specie].[Konvensjonell]}, {[Measures].[Value]}) ON COLUMNS, 
  NON EMPTY [~ROWS] ON ROWS FROM [NoTrade] 
  WHERE CrossJoin({[Flow].[Flow].[export]}, {[Time].[Time].[2015]})")
colnames(result.raw)[1] <- "Partner";
colnames(result.raw)[2] <- "Iso3";
colnames(result.raw)[3] <- "Opdrett";
colnames(result.raw)[4] <- "Konvensjonell";
result.melt <- melt(result.raw)
ggplot(result.melt, aes(x=Partner, y=value, fill=variable)) + 
  ggtitle('Conventional VS Farmed exports from Norway 2015') +
  geom_bar(position = 'fill', stat = 'identity') +
  scale_fill_brewer(palette = 'Pastel1') +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))
```
![Norwegian exports of farmed vs conventional to top 30 countries plot](https://bitbucket.org/capia_as/rmdx/raw/master/no_exp_conv_vs_farmed.png)

### Documentation:
```R
help(RMDX)
```

[Mondrian MDX specification](http://mondrian.pentaho.com/documentation/mdx.php)

[Microsoft, the origin of MDX has some nice documentation](https://msdn.microsoft.com/en-us/library/ms145506.aspx), but not everything is supported.

### Bugs:
You can file bugs to this project by emailing me at stian@capia.no

#### Known issues:
There are some OSs (Arch, Ubuntu, OpenSuse) that does not have option set for unzip.

[Devtools issue#406](https://github.com/hadley/devtools/issues/406)
```R
getOption("unzip")
# CORRECT
[1] "internal"
# WRONG
[1] ""
```
Fix this by running this line before trying normal installation again.
```R
options(unzip = 'internal') 
```

### License:
RMDX is Copyright (c) 2015 - and beyond Piers Harding.
It is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

A copy of the GNU Lesser General Public License (version 3) is included in
the file LICENSE.